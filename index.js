const form = document.querySelector('.js--form');
const widthInput = form.querySelector('.js--image-width');
const heightInput = form.querySelector('.js--image-height');
const galleryContainer = document.querySelector('.js--gallery');
const spinner = document.querySelector('.js--spinner');
const btnLoad = document.querySelector('.js--button-load');

let galleryMainImage = null;
let gallerySub = null;
let isLoading = false;
let isError = false;
let activeIndex = 0;

const errors = {
    empty: 'This field must be filled',
    number: 'Only numbers can be entered'
};

const clearErrors = () => {
    const errorWrappers = document.querySelectorAll('.js--form-input-error');
    
    isError = false;

    errorWrappers.forEach(item => {
        item.classList.add('hidden');
        item.textContent = '';
    });
}

const validateInput = (inputEl) => {
    const val = inputEl.value;
    const parent = inputEl.parentNode;
    const errorElem = parent.querySelector('.js--form-input-error');

    if (!val) {
        errorElem.textContent = errors.empty;
        errorElem.classList.remove('hidden');
        isError = true;

        return;
    }

    if (isNaN(val)) {
        errorElem.textContent = errors.number;
        errorElem.classList.remove('hidden');
        isError = true;

        return;
    }
};

const setLoadingStatus = (done) => {
    if (done) {
        spinner.classList.add('hidden');
        btnLoad.disabled = false;
        isLoading = false;

        return;
    }

    spinner.classList.remove('hidden');
    btnLoad.disabled = true;
    isLoading = true;
};

const generateRandomId = () => {
    return Math.random().toString(36).substring(2, 10) + Math.random().toString(36).substring(2, 10);
};

const generateRandomIdsArray = () => {
    const idsArray = [];

    for (let index = 0; index < 4; index++) {
        idsArray.push(generateRandomId());   
    }

    return idsArray;
};

const getImages = async (ids) => {
    const result = [];
    setLoadingStatus(false);

    const arrayFetchImages = ids.map(img => fetch(`https://picsum.photos/seed/${img}/${widthInput.value}/${heightInput.value}`));

    await Promise.all(arrayFetchImages)
        .then((responses) => {
            responses.map((res) => {
                result.push(res.url);
                setLoadingStatus(true);
            })
        }).catch((err) => {
            alert(err);
            console.log(err);
            setLoadingStatus(true);
        });

    return result;
};

const setMainImage = (src) => {
    const imgElem = galleryMainImage.querySelector('img');

    imgElem.src = src;
    imgElem.width = widthInput.value;
    imgElem.height = heightInput.value;
};

const clearActiveClassGallery = () => {
    const nodes = document.querySelectorAll('.gallery-sub-item');

    nodes.forEach(item => {
        if (item.classList.contains('active')) item.classList.remove('active');
    })
};

const createMainImage = (src) => {
    const imgElement = document.createElement('img');
    imgElement.src = src;

    galleryMainImage = document.createElement('div');
    galleryMainImage.classList.add('gallery-main');

    galleryMainImage.append(imgElement);
    galleryContainer.append(galleryMainImage);
};

const createSubGallery = (imgsArray) => {
    gallerySub = document.createElement('div');
    gallerySub.classList.add('gallery-sub', 'mt-3');

    for (let index = 0; index < imgsArray.length; index++) {
        const imgElem = document.createElement('img');
        const wrapper = document.createElement('button');
        const src = imgsArray[index];

        imgElem.src = src;
        imgElem.width = widthInput.value;
        imgElem.height = heightInput.value;

        wrapper.classList.add('gallery-sub-item');
        wrapper.dataset.index = index;

        if (index === 0) wrapper.classList.add('active');

        wrapper.append(imgElem);
        gallerySub.append(wrapper);
        galleryContainer.append(gallerySub);

        wrapper.addEventListener('click', (e) => {
            const { target } = e;
            const elementIndex = target.dataset.index;
            if (activeIndex === elementIndex) return;

            clearActiveClassGallery();
            target.classList.add('active');
            setMainImage(imgsArray[elementIndex]);
            activeIndex = elementIndex;
        });
    }
};

form.addEventListener('submit', async (e) => {
    e.preventDefault();

    if (isLoading) return;

    clearErrors();
    validateInput(widthInput);
    validateInput(heightInput);

    if (isError) return;

    activeIndex = 0;
    galleryContainer.textContent = '';
    galleryContainer.classList.add('hidden');

    const idsArray = generateRandomIdsArray();
    const imgsArray = await getImages(idsArray);
    const [first] = imgsArray;

    createMainImage(first);
    createSubGallery(imgsArray);

    galleryContainer.classList.remove('hidden');
});

// https://picsum.photos/seed/random_id/image_width/image_height